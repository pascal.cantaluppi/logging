import { Injectable } from '@nestjs/common';
import { createLogger } from 'winston';
import { Papertrail } from 'winston-papertrail';

@Injectable()
export class AppService {
  loggerWinston: any;
  papertrail: Papertrail;

  constructor() {
    this.papertrail = new Papertrail({
      host: 'logs5.papertrailapp.com',
      port: 53241,
      program: 'POC',
      inlineMeta: true,
    });
    this.loggerWinston = createLogger({
      transports: [this.papertrail],
    });
  }

  getHello(): string {
    this.loggerWinston.info('[NestJS Response] Hello world');
    return 'Hello World!';
  }
}
