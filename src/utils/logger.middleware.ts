/* eslint-disable prettier/prettier */
import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { createLogger } from 'winston';
import { Papertrail } from 'winston-papertrail';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  private papertrail = new Papertrail({
    host: 'logs5.papertrailapp.com',
    port: 53241,
    program: 'POC',
    inlineMeta: true,
  });
  private loggerWinston = createLogger({
    transports: [this.papertrail],
  });

  use(request: Request, response: Response, next: NextFunction): void {
    const { ip, method, originalUrl } = request;
    const userAgent = request.get('user-agent') || '';
    response.on('finish', () => {
      const { statusCode } = response;
      const contentLength = response.get('content-length');
      const content = `${method} ${originalUrl} ${statusCode} ${contentLength} - ${userAgent} ${ip}`;
      this.loggerWinston.info(content);
      if (method !== 'GET') {
        this.loggerWinston.info(request.body);
      }
    });
    next();
  }
}
