# Logger Playground

## Logging a NestJS Backend

<img src="https://gitlab.com/pascal.cantaluppi/logging/-/raw/master/img/readme.png" width="400" />

Proof of concept for collecting api data with winston and store it on solarwinds papertrail.

## Setup

```bash
$ npm i
```

## Run

```bash
$ npm run start:dev
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
